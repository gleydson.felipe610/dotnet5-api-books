# dotnet5-api-books

[![pipeline status](https://gitlab.com/devlabops/dotnet5-api-books/badges/master/pipeline.svg)](https://gitlab.com/devlabops/dotnet5-api-books/-/commits/master)

REST API | .NET 5.0 e MongoDB


*API que executa operações de Criar, Ler, Atualizar e Excluir (CRUD) em um banco de dados MongoDB*

## Pré-requisitos / Versões

- .NET 5.0 / 5.0.400
- MongoDB / latest:docker
- Docker Engine / 20.10.8
- docker-compose version / 1.29.2
- curl / 7.68.0
### Executar localmente

Para executar localmente é necessário ter **.NET 5** instalado e o **MongoDB**. Recomendo fortemente utilizar o docker para subir o MongoDB. A configuração da conexão da API com o banco de dados fica no arquivo **appsettings.json**

1. Subir o MongoDB usando o docker:

```sh
docker run -d -p 27017:27017 mongo:latest
```

2. Essa configuração já está **pronta** mas é importante destacar que ela é feita no arquivo *appsettings.json*. Qualquer configuração relacionada a String de conexão é feita nesse arquivo(usuário, senha, url do banco de dados, porta..)

```
"BookstoreDatabaseSettings": {
    "BooksCollectionName": "Books",
    "ConnectionString": "mongodb://localhost:27017",
    "DatabaseName": "BookstoreDb"
  }
```

> Nesse exemplo só definimos o Database, o nome da Colletion e o endereço que será localhost na porta 27017(default).

3. Agora restaurar e rodar o projeto com os comandos:

```sh
dotnet restore
dotnet run
```

Para verificar se tudo ocorreu bem basta acessar a página do Swagger e tentar fazer uma inserção no banco de dados. Você pode fazer a mesma operação utilizando o *curl*:

4. Para abrir o Swagger no navegador:

```
http://localhost:4000/swagger/index.html
```

![swagger](https://gitlab.com/devlabops/api-flavors/dotnet5-api-books/-/raw/master/.devlabops/swagger.png)

5. Fazer uma inserção e uma consulta no banco usando o *curl*:

```
curl -X POST "http://localhost:4000/api/Books" -H  "accept: text/plain" -H  "Content-Type: application/json-patch+json" -d "{\"id\":\"string\",\"bookName\":\"test\",\"price\":20,\"category\":\"category1\",\"author\":\"author1\"}"

curl -X GET "http://localhost:4000/api/Books" -H  "accept: text/plain"
```

Até agora configuramos nossa API para rodar localmente na nossa máquina(*modo desenvolvimento*), ela também está utilizando um banco de dados que subimos utilizando o Docker. O objetivo dos próximos passos é fazer nossa API rodar dentro de um container Docker, assim como nosso banco de dados. Para isso alguns ajustes devem ser feitos como o apontamento do banco de dados.

### Conteinerizar a API

Assim como executamos o MongoDB anteriormente vamos executar nossa API dentro de um container. Esse exemplo utiliza o Docker. Isso facilita a distribuição, portabilidade e gerenciamento da nossa aplicação. O primeiro passo é criar o arquivo **Dockerfile** que contém todos os passos necessários para mover e executar nosso código para dentro do container:

Dockerfile:
```
FROM mcr.microsoft.com/dotnet/aspnet:5.0

WORKDIR /app
EXPOSE 4000
COPY publish_output .
ENTRYPOINT ["dotnet", "BooksApi.dll"]
```

> Detalhes sobre o Dockerfile podem ser encontrados na documentação oficial do Docker

É necessário configurar o nosso arquivo *appsettings.json* para apontar para o *service* do nosso MongoDB. Ou seja, vamos parar de apontar para **localhost** e vamos apontar para o service do MongoDB ou IP(não recomendado). Para esse exemplo basta substituir a linha que contém *ConnectionString* para: 

```
"ConnectionString": "mongodb://devlabops-mongodb:27017",
```

Nossa string de conexão agora aponta para **devlabops-mongodb**, mas ainda não temos esse service de pé

> 'devlabops-mongodb' pode ser substituído por qualque nome a sua escolha, mas lembre de substituir em todos os lugares(API, docker-compose, comando docker..)

Antes de partir para parte do Docker vale lembrar que estamos trabalhando com o .NET 5. Cada linguagem/framework possui suas próprias particularidades. No caso do .NET 5 precisamos fazer o **build**, ou seja, compilar o projeto e suas dependências em um conjunto de binários. Podemos fazer isso com o comando:

```
dotnet publish -c Release -o publish_output
```

Esse comando gera uma pasta chamada *publish_output*. Essa pasta é o artefato final que vamos utilizar dentro do nosso container. Para qualquer alteração que você fizer no código será necessário fazer um novo build e consequentemente gerar uma nova *publish_output*. Agora vamos criar uma imagem que irá utilizar de base uma imagem do .NET compatível com nosso código, para isso basta executar:

```
docker build -t dotnet5-api-books .
```

Para rodar o container com a imagem gerada acima:

```
docker run -p 4000:4000 -d dotnet5-api-books
```

Agora temos nossa API rodando dentro de um container Docker. Se o comando acima funcionar você pode acessar aquela mesma página do **Swagger** mas logo vai perceber que irá ter problemas ao executar as mesmas operações de *inserção e consulta*. Isso acontence porque alteramos nossa string de conexão no arquivo *appsettings.json* e agora nossa aplicação não reconhece o MongoDB(*devlabops-mongodb*). Vamos então apagar os nossos containers e mostrar duas abordagens para resolver esse problema: 

Apagar todos os containers. CUIDADO !

```
docker container rm -f $(docker container ls -a -q)
```

**Abordagem 1**: Rodar criando links entre os containers, onde vamos subir o MongoDB com o nome que configuramos na nossa string de conexão(*devlabops-mongodb*). O primeiro comando criar o **MongoDB** e o segundo sobe nossa **API** com um link com o nome conhecido que referencia o container do banco.

```
docker run -d --name devlabops-mongodb mongo:latest
docker run -d -P --name dotnet5-api-books -p 4000:4000 --link devlabops-mongodb:devlabops-mongodb  dotnet5-api-books
```

**Abordagem 2**: Criar um arquivo chamado *docker-compose.yaml* e criar definir o serviço do MongoDB como devlabops-mongodb:

```
version: '3'

services:
  devlabops-mongodb:
    image: mongo
    restart: always
    ports:
      - "27017:27017"
    networks: 
      - network-test

  dotnet5-api-books:
    image: dotnet5-api-books:latest
    ports:
      - "4000:4000"
    networks: 
      - network-test
    links:
      - devlabops-mongodb
    depends_on:
      - devlabops-mongodb
      
networks: 
  network-test:
      driver: bridge
```

O arquivo **docker-compose.yaml** cria os serviços para o MongoDB e nossa API, ele também expoem suas portas na máquina Host, além disso cria uma network que permite a comunicação entre esses containers. Para subir nossa stack (API + MongoDB) basta executar:

```
docker-compose up -d
```

Se tudo ocorrer bem basta fazer os mesmos testes com curl ou Swagger. Você pode visualizar os containers com o comando:

```
docker-compose ps
```

### Objeto que a API cria no MongoDB

Um exemplo do objeto:

```javascript
{
  "_id" : ObjectId("5bfd996f7b8e48dc15ff215d"),
  "Name" : "Design Patterns",
  "Price" : 54.93,
  "Category" : "Computers",
  "Author" : "Ralph Johnson"
}
{
  "_id" : ObjectId("5bfd996f7b8e48dc15ff215e"),
  "Name" : "Clean Code",
  "Price" : 43.15,
  "Category" : "Computers",
  "Author" : "Robert C. Martin"
}
```
### Alteração da porta da API

Para alterar a porta da API edite o arquivo: *Program.cs*

```
WebHost.CreateDefaultBuilder(args)
    .UseStartup<Startup>()
    .UseUrls(urls: "http://0.0.0.0:4000")
    .Build();
```

> Padrão: 4000

### Inserir um objeto manualmente no MongoDB

Para inserir um objeto no banco de dados ser utilizar API:

```
mongo
use BookstoreDb
db.Books.insertMany([{'BookName':'Design Patterns','Price':54.93,'Category':'Computers','Author':'Ralph Johnson'}, {'BookName':'Clean Code','Price':43.15,'Category':'Computers','Author':'Robert C. Martin'}])
```

### Requisições fake

```
chmod +x fake-requests.sh
./fake-requests.sh
```

### Autor

- @apolzek